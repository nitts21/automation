package com.phptravels.testCases;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.phptravels.base.TestBase;
import com.phptravels.constants.ProjectConstants;
import com.phptravels.pages.LoginPage;
import com.phptravels.pages.MyAccountPage;

public class LoginPageTest extends TestBase{
	private LoginPage loginPage;
	private MyAccountPage myAccountPage;

	
	public LoginPageTest() {
		super();
	}
	
	@BeforeTest
	public void setUp() {
		initialization();
		loginPage=new LoginPage();
	}
	
	@Test(priority=1)
	public void testpageTitle() {
		Assert.assertEquals("Login", loginPage.validateLoginPageTitle());
	}
	
	@Test(priority=2)
	public void testLoginPage() throws InterruptedException {
		myAccountPage=loginPage.loginToTravels(prop.getProperty(ProjectConstants.USERNAME), prop.getProperty(ProjectConstants.PASSWORD),true);
		Assert.assertNotNull(myAccountPage);
	}
	
	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}
