package com.phptravels.testCases;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.phptravels.base.TestBase;
import com.phptravels.constants.ProjectConstants;
import com.phptravels.pages.LoginPage;
import com.phptravels.pages.MyAccountPage;

public class MyAccountPageTest extends TestBase {
	private MyAccountPage myAccountPage;
	private LoginPage loginPage;
	
	public MyAccountPageTest() {
		super();
	}
	
	@BeforeTest
	public void setUp() throws InterruptedException {
		initialization();
		myAccountPage=new MyAccountPage();
		loginPage = new LoginPage();
		myAccountPage=loginPage.loginToTravels(prop.getProperty(ProjectConstants.USERNAME), prop.getProperty(ProjectConstants.PASSWORD),false);
	}
	
	@Test(priority=1)
	public void testPageTitle() {
		String pageTitle=myAccountPage.getPageTitle();
		Assert.assertEquals(pageTitle, "My Account");
	}
	
	@Test(priority=2)
	public void testLoginPersonDetails() {
		String loginPersonDetails=myAccountPage.validateLoginPerson();
		Assert.assertEquals(loginPersonDetails, "Hi, Johny Smith");
	}
	
	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}
