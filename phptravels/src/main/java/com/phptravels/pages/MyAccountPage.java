package com.phptravels.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.phptravels.base.TestBase;

public class MyAccountPage extends TestBase {
	
	@FindBy(className="RTL")
	WebElement rtlElement;
	
	public MyAccountPage() {
		PageFactory.initElements(driver, this);
	}
	
	public String getPageTitle() {
		return driver.getTitle();
	}
	
	public String validateLoginPerson() {
		return rtlElement.getText();
	}

}
