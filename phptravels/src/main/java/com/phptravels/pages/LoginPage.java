package com.phptravels.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.phptravels.base.TestBase;

public class LoginPage extends TestBase {

	@FindBy(xpath = "//a[contains(text(),'My Account')]")
	List<WebElement> myAccountElement;

	@FindBy(xpath = "//a[@href='https://www.phptravels.net/login']")
	List<WebElement> loginUrlElement;

	@FindBy(name = "username")
	WebElement userNameElement;

	@FindBy(name = "password")
	WebElement passwordElement;

	@FindBy(xpath = "//button[contains(text(),'Login')]")
	WebElement loginElement;

	/**
	 * This is being used to intialize the page factory.
	 */
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	/**
	 * This method returns the current page title.
	 * 
	 * @return
	 */
	public String validateLoginPageTitle() {
		navigatesToLoginPage();
		return driver.getTitle();
	}

	/**
	 * This method returns the page object of MyAccount page.
	 * 
	 * @param userName
	 * @param password
	 * @param loginFromLoginPage
	 * @return
	 */
	public MyAccountPage loginToTravels(String userName, String password, Boolean loginFromLoginPage) {
		if (!loginFromLoginPage) {
			navigatesToLoginPage();
		}
		userNameElement.sendKeys(userName);
		passwordElement.sendKeys(password);
		loginElement.click();
		return new MyAccountPage();
	}

	/**
	 * Navigates to login page from landing page.
	 */
	private void navigatesToLoginPage() {
		myAccountElement.get(1).click();
		loginUrlElement.get(1).click();
	}

}
