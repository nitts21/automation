package com.phptravels.constants;

public class ProjectConstants {
	
	private ProjectConstants() {
		
	}
	
	public static final int PAGE_LOAD_TIMEOUT_CONSTANTS=60;
	public static final int IMPLICIT_WAIT_CONSTANTS=60;
	public static final String USERNAME="username";
	public static final String PASSWORD="password";

}
