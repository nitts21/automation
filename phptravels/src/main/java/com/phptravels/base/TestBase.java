package com.phptravels.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.phptravels.constants.ProjectConstants;

public class TestBase {
	protected static WebDriver driver;
	protected static Properties prop;
	private static final String BROWSER = "browser";
	private static final String URL = "url";
	private static final Logger log = Logger.getLogger(TestBase.class);

	/**
	 * Constructor to load config properties value.
	 */
	protected TestBase() {
		prop = new Properties();
		log.info("Inside TestBase class");
		StringBuilder sb = new StringBuilder();
		sb.append(System.getProperty("user.dir"));
		sb.append("\\src\\main\\resources\\config.property");
		try (FileInputStream inputStream = new FileInputStream(sb.toString());) {
			prop.load(inputStream);
		} catch (FileNotFoundException e) {
			log.error("Unable to load config property file : ", e);
		} catch (IOException e) {
			log.error("IOException encountered while loading property file : ", e);
		}
	}

	/**
	 * Initialize the webdriver based on the browser name defined in the config
	 * file.
	 */
	protected static void initialization() {
		String browserName = prop.getProperty(BROWSER);
		StringBuilder driverBuilder = new StringBuilder();
		driverBuilder.append(System.getProperty("user.dir"));
		driverBuilder.append("//src//main//resources//chromedriver.exe");
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", driverBuilder.toString());
			driver = new ChromeDriver();
		}

		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(ProjectConstants.PAGE_LOAD_TIMEOUT_CONSTANTS, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(ProjectConstants.IMPLICIT_WAIT_CONSTANTS, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(prop.getProperty(URL));
	}

}
